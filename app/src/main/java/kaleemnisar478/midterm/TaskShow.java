package kaleemnisar478.midterm;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class TaskShow extends AppCompatActivity {

    ShowDetailsDialog dialog;
    View v;
    private TaskAdapter adapter;
    private List<Task> taskList = new ArrayList<>();
    private FrameLayout frameLayout;
    private RecyclerView recyclerView;
    private TextView noUserView;

    DatabaseReference reference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_show);

        reference= FirebaseDatabase.getInstance().getReference("Tasks");

        recyclerView = findViewById(R.id.recycler_view);
        noUserView = findViewById(R.id.empty_notes_view);




        adapter = new TaskAdapter(getApplicationContext(), taskList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getApplicationContext(), LinearLayoutManager.VERTICAL, 16));
        recyclerView.setAdapter(adapter);
        toggleEmptyUsers();

        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                taskList.clear();
                for (DataSnapshot postSnapshot : snapshot.getChildren()) {
                    Task t = postSnapshot.getValue(Task.class);
                    taskList.add(t);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(),
                recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                Task task1=taskList.get(position);
                showActionsDialog(task1,"view");

            }

            @Override
            public void onLongClick(View view, int position) {
                Task task1=taskList.get(position);
                showActionsDialog(task1,"edit");

            }
        }));


    }


    private void showActionsDialog(Task t,String s) {
        dialog=new ShowDetailsDialog(t,s);
        dialog.show(getSupportFragmentManager(),"Task dialog");// use getChild... see from a link
        dialog.setCancelable(false);     // dialog should not close on touches outside the dialog (and using this another case by default on back press dialog not closes)
        //  dialog.setCanceledOnTouchOutside(false); not work in fragment
        // dialog.getDialog().getButton(AlertDialog.BUTTON_POSITIVE)
    }


    private void toggleEmptyUsers() {
        // you can check notesList.size() > 0

//        if (db.getUsersCount() > 0) {
//            noUserView.setVisibility(View.GONE);
//        } else {
//            noUserView.setVisibility(View.VISIBLE);
//        }
    }
}


