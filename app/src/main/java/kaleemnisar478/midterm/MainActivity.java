package kaleemnisar478.midterm;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.google.android.material.datepicker.MaterialDatePicker;
import com.google.android.material.datepicker.MaterialPickerOnPositiveButtonClickListener;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class MainActivity extends AppCompatActivity {

    TextInputLayout task,taskDetails,deadline;
    Button check,add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        task=findViewById(R.id.task);
        taskDetails=findViewById(R.id.taskDetails);
        deadline=findViewById(R.id.deadline);
        add=findViewById(R.id.register);
        check=findViewById(R.id.checktask);




        MaterialDatePicker.Builder builder = MaterialDatePicker.Builder.datePicker();
        builder.setTitleText("SELECT A DATE");
        final MaterialDatePicker mdp = builder.build();
        deadline.getEditText().setKeyListener(null);
        mdp.addOnPositiveButtonClickListener(new MaterialPickerOnPositiveButtonClickListener() {
            @Override
            public void onPositiveButtonClick(Object selection) {
                deadline.getEditText().setText(mdp.getHeaderText());
            }
        });
        deadline.getEditText().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mdp.show(getSupportFragmentManager(),"Dead Line Picker");
            }
        });







        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if( !validateDeadline() | !validateTaskDetails()| !validateTask()){
                    Toast.makeText(getApplicationContext(),"Fill all Fields with no errors",Toast.LENGTH_LONG).show();
                    return;
                }
                addData();
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(getApplicationContext(),TaskShow.class);
                startActivity(intent);
            }
        });



    }

    public void addData()
    {



//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("message");
//
//        myRef.setValue("Hello, World!");

        FirebaseDatabase  rootNode = FirebaseDatabase.getInstance();
        DatabaseReference reference = rootNode.getReference("Tasks");


        String taskName=task.getEditText().getText().toString();
        String taskDetail=taskDetails.getEditText().getText().toString();
        String dead=deadline.getEditText().getText().toString();

        String id = reference.push().getKey();
        Task t=new Task(id,taskName,taskDetail,dead);


        reference.child(id).setValue(t);


         Toast.makeText(getApplicationContext(),"Task Added Successfully",Toast.LENGTH_LONG).show();
         task.getEditText().setText("");
         taskDetails.getEditText().setText("");
         deadline.getEditText().setText("");





    }

    private boolean validateTask(){
        String val=task.getEditText().getText().toString();
        if(val.isEmpty()){
            task.setError("Field cannot be empty");
            task.requestFocus();
            return false;
        }
        return true;
    }

    private boolean validateTaskDetails(){
        String val=taskDetails.getEditText().getText().toString();
        if(val.isEmpty()){
            taskDetails.setError("Field cannot be empty");
            taskDetails.requestFocus();
            return false;
        }
        return true;
    }


    private boolean validateDeadline(){
        String val=deadline.getEditText().getText().toString();
        if(val.equals("DD/MM/YYYY")){
            deadline.setError("Select date");
            deadline.requestFocus();
            return false;
        }
        return true;
    }

}