package kaleemnisar478.midterm;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatDialogFragment;
import androidx.constraintlayout.widget.ConstraintLayout;

public class ShowDetailsDialog extends AppCompatDialogFragment {

    private Task t;
    private String state;
    private ConstraintLayout constraintLayout;

    private TextInputLayout task,taskdet,deadline;
    private Button add,check;


    private AlertDialog.Builder builder=null;
    private AlertDialog mAlertDialog=null;


    public ShowDetailsDialog(Task task,String s) {
        this.t = task;
        this.state=s;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {

        builder=new AlertDialog.Builder(getActivity());
        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view=inflater.inflate(R.layout.activity_main,null);

        task=view.findViewById(R.id.task);
        taskdet=view.findViewById(R.id.taskDetails);
        deadline=view.findViewById(R.id.deadline);

        add=view.findViewById(R.id.register);
        check=view.findViewById(R.id.checktask);
        constraintLayout=view.findViewById(R.id.register_user_Layout);


        add.setVisibility(View.GONE);
        check.setVisibility(View.GONE);

        task.getEditText().setText(t.getTask());
        taskdet.getEditText().setText(t.getTaskDetails());
        deadline.getEditText().setText(t.getDeadline());


        constraintLayout.setPadding(50,20,50,20);

        //disableEdit();

        if (state.equals("edit")) {
            enableEdit();
            builder.setView(view)
                    .setTitle("Task Details")
                    .setNegativeButton("Delete", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            deleteData(t);

                        }
                    }).setPositiveButton("Update", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    String taskname = task.getEditText().getText().toString();
                    String details = taskdet.getEditText().getText().toString();
                    String date = deadline.getEditText().getText().toString();

                    Task updateTask = new Task(t.getId(), taskname, details, date);

                    updateData(updateTask);
                }
            }).setNeutralButton("ok", null);
        }
        else
        {
            disableEdit();
            builder.setView(view)
                    .setTitle("Task Details")
                    .setNegativeButton("ok",null);
        }



        mAlertDialog= builder.create();
        //mAlertDialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;

         mAlertDialog.show();


        return mAlertDialog;

    }


    void disableEdit()
    {
        task.setEnabled(false);
        taskdet.setEnabled(false);
        deadline.setEnabled(false);

    }

    void enableEdit()
    {
        task.setEnabled(true);
        taskdet.setEnabled(true);
        deadline.setEnabled(true);

    }

    void updateData(Task t)
    {
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Tasks").child(t.getId());
        databaseReference.setValue(t);
    }


    void deleteData(Task t)
    {
        DatabaseReference databaseReference= FirebaseDatabase.getInstance().getReference("Tasks").child(t.getId());
        databaseReference.removeValue();
    }



}
