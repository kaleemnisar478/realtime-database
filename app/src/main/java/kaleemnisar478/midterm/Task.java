package kaleemnisar478.midterm;

import android.graphics.Bitmap;

public class Task {



    private String id;
    private String task;
    private String taskDetails;
    private String deadline;




    public Task() {
    }

    public Task(String id, String task, String taskDetails, String deadline) {
        this.id = id;
        this.task = task;
        this.taskDetails = taskDetails;
        this.deadline = deadline;
    }

    public Task(String task, String taskDetails, String deadline) {
        this.task = task;
        this.taskDetails = taskDetails;
        this.deadline = deadline;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getTaskDetails() {
        return taskDetails;
    }

    public void setTaskDetails(String taskDetails) {
        this.taskDetails = taskDetails;
    }

    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }
}
